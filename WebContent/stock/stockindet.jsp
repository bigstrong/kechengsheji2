<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<%=path %>/css/table.css"  rel="stylesheet"/>
<title>无标题文档</title>
<style>
   a:active{ border:1px solid #000}
</style>
</head>

<body><br />
<div class="main">
<input type="text" />
 <a href="#"><img src="<%=path %>/image/search.png" />查询</a>
 <a href="#"><img  src="<%=path %>/image/add.png"/>添加</a>
</div>
<table class="table-integral main">
  <thead>
   <tr>
     <td>入库详细编号</td>
     <td>入库编号</td>
     <td>入库商品</td>
     <td>入库数量</td>
   </tr>
   </thead>
   
   <tbody>
   <c:forEach items="${list}" var="stockindet">
   <tr>
     <td>${stockindet.stockindet_id}</td>
     <td>${stockindet.stockin_id}</td>
     <td>${stockindet.pro_name}</td>
     <td>${stockindet.stockindet_count}</td>
   </tr>
   </c:forEach>
   </tbody>
</table>
<div class="main">
  <div id="wrap" class="page_btn clear">
 
  </div>
</div>
</body>
</html>