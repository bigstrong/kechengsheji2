package com.strong.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.tb_stockin;
import com.strong.bean.tb_stockinDet;
import com.strong.bean.tb_stockout;
import com.strong.bean.tb_stockoutdet;
import com.strong.dao.Tb_stockinDao;
import com.strong.dao.Tb_stockinDetDao;
import com.strong.dao.Tb_stockoutDao;
import com.strong.dao.Tb_stockoutDetDao;

/**
 * Servlet implementation class AddStockoutServlet
 */
@WebServlet("/AddStockoutServlet")
public class AddStockoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddStockoutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setCharacterEncoding("utf-8");
		String message = "";// 操作完成的提示消息
//		String path = "ColorServlet?type=query";//
		String path = "StockoutServlet?page=1&pageSize=5";
		int huame=Integer.parseInt(request.getParameter("hname")); //获取仓库ID
		int uname=Integer.parseInt(request.getParameter("uname"));//获取用户ID
		int pname=Integer.parseInt(request.getParameter("pname"));//获取商品ID
		int cname=Integer.parseInt(request.getParameter("clist"));      //获取客户ID
		int sum=Integer.parseInt(request.getParameter("sum"));			//获出库数量
		String remark = request.getParameter("remark");							//获取评论
		
		System.out.println(uname);
		System.out.println(sum);
		System.out.println(remark);
		java.util.Date now=new java.util.Date();
        java.sql.Date now1=new java.sql.Date(now.getTime());
        
		//首先插出库表
		tb_stockout insert = new tb_stockout();
		insert.setWare_id(huame);
		insert.setUser_id(uname);
		insert.setClient_id(cname);
		insert.setStockout_date(now1);
		insert.setStockout_remark(remark);
		
		Tb_stockoutDao Stockoutdao = new Tb_stockoutDao();
		int i = Stockoutdao.insert(insert);
		if (i > 0) {
			message = "添加成功!";
			 List<tb_stockout> infro = new ArrayList<tb_stockout>();
			 infro = Stockoutdao.queryAll();
//			 System.out.println(infro.get(infro.size()-1));
			 int stockoutid = infro.get(infro.size()-1).getStockout_id();   //获取出库的ID
			 tb_stockoutdet inferdet = new tb_stockoutdet();
			 Tb_stockoutDetDao detdao = new Tb_stockoutDetDao();
			 inferdet.setStockoutdet_count(sum);
			 inferdet.setStockout_id(stockoutid);
			 inferdet.setPro_id(pname);
			 int j = detdao.insert(inferdet);
			 System.out.println(j);
		} else {
			message = "添加失败";
		}
		
		 
		
		request.setAttribute("message", message);
		request.setAttribute("path", path);
		request.getRequestDispatcher("success.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
