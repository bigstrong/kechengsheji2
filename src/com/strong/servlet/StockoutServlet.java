package com.strong.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.PageBean;
import com.strong.bean.StockoutView;
import com.strong.dao.Tb_stockoutDao;

/**
 * Servlet implementation class StockoutServlet
 */
@WebServlet("/StockoutServlet")
public class StockoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockoutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());

		  int page=Integer.parseInt(request.getParameter("page"));//当前页码
        int pageSize=Integer.parseInt(request.getParameter("pageSize"));//页面显示的大小
        int count;
		   PageBean<StockoutView> pageBean=new PageBean<StockoutView>();
        Tb_stockoutDao dao=new Tb_stockoutDao();
        List<StockoutView> list=dao.queryAllView(page, pageSize);
        count = dao.queryAllView().size();
        request.setAttribute("list", list);
		request.setAttribute("page", page);
		request.setAttribute("count", count);
//        pageBean.setCount(dao.queryAllView().size());
//        pageBean.setList(list);
//        pageBean.setPage(page);
//request.setAttribute("pageBean", pageBean);
        request.getRequestDispatcher("stock/stockout.jsp").forward(request, response);
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
