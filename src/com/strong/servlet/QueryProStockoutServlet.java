package com.strong.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.tb_client;
import com.strong.bean.tb_product;
import com.strong.bean.tb_user;
import com.strong.bean.tb_warehouse;
import com.strong.dao.Tb_clientDao;
import com.strong.dao.Tb_productDao;
import com.strong.dao.Tb_userDao;
import com.strong.dao.Tb_warehouseDao;

/**
 * Servlet implementation class QueryProStockoutServlet
 */
@WebServlet("/QueryProStockoutServlet")
public class QueryProStockoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryProStockoutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        //查仓库
//		Tb_colorDao colorDao=new Tb_colorDao();
//        List<Tb_color> list=colorDao.queryAll();
		Tb_warehouseDao  wdao = new Tb_warehouseDao();
		List<tb_warehouse> houselist=wdao.queryAll();
        //查用户
		Tb_userDao  udao = new Tb_userDao();
		List<tb_user> ulist=udao.queryAll();
        //查商品
		Tb_productDao pdao = new Tb_productDao();
		List<tb_product>  plist = pdao.queryAll();
		//查客户
		Tb_clientDao cdao = new Tb_clientDao();
		List<tb_client> clist = cdao.queryAll();
		
//        request.setAttribute("colorList", list);
		request.setAttribute("houselist" ,houselist);
		request.setAttribute("ulist" ,ulist);
		request.setAttribute("plist" ,plist);
		request.setAttribute("clist",clist);
		
        request.getRequestDispatcher("system/addStockout.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
