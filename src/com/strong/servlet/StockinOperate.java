package com.strong.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.StockinView;
import com.strong.bean.tb_color;
import com.strong.bean.tb_stockin;
import com.strong.dao.Tb_colorDao;
import com.strong.dao.Tb_stockinDao;

/**
 * Servlet implementation class StockinOperate
 */
@WebServlet("/StockinOperate")
public class StockinOperate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockinOperate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String type = request.getParameter("type");// 获取type的类型
		response.setCharacterEncoding("utf-8");
		String message = "";// 操作完成的提示消息
//		String path = "ColorServlet?type=query";//
		String path = "StockinServlet?page=1&pageSize=5";
		int i = 0;// 表示 增删改默认的返回值
		// 删除的操作
				if ("delete".equals(type)) {
					int id = Integer.parseInt(request.getParameter("stockinId"));// 将字符串转换成int类型
					Tb_stockinDao dao = new Tb_stockinDao();
					i = dao.delete(id);
					if (i > 0) {
						message = "删除成功!";
					} else {
						message = "删除失败";
					}

				}
				//根据颜色名字模糊查询
				if("queryByName".equals(type)){
					String name=request.getParameter("name");
					System.out.println(name);
//					name = new String(name.getBytes("iso-8859-1"), "utf-8");
					Tb_stockinDao dao=new Tb_stockinDao();
					List<StockinView> stockinlist =dao.queryByName(name);
					request.setAttribute("list", stockinlist);// 将查询的数据放入 request中
					// 服务器转发
					request.getRequestDispatcher("stock/stockin.jsp").forward(request,
							response);
					return;	
				}

				
				request.setAttribute("message", message);
				request.setAttribute("path", path);
				request.getRequestDispatcher("success.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
