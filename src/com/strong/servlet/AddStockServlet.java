package com.strong.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.tb_stockin;
import com.strong.bean.tb_stockinDet;
import com.strong.dao.Tb_stockinDao;
import com.strong.dao.Tb_stockinDetDao;

/**
 * Servlet implementation class AddStockServlet
 */
@WebServlet("/AddStockServlet")
public class AddStockServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddStockServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setCharacterEncoding("utf-8");
		String message = "";// 操作完成的提示消息
//		String path = "ColorServlet?type=query";//
		String path = "StockinServlet?page=1&pageSize=5";
		int huame=Integer.parseInt(request.getParameter("hname")); //获取仓库ID
		int uname=Integer.parseInt(request.getParameter("uname"));//获取用户ID
		int pname=Integer.parseInt(request.getParameter("pname"));//获取商品ID
		int sum=Integer.parseInt(request.getParameter("sum"));			//获取入库数量
		String remark = request.getParameter("remark");							//获取评论
		
		System.out.println(uname);
		System.out.println(sum);
		System.out.println(remark);
		java.util.Date now=new java.util.Date();
        java.sql.Date now1=new java.sql.Date(now.getTime());
        
		//首先插入入库表
		tb_stockin insert = new tb_stockin();
		insert.setWare_id(huame);
		insert.setUser_id(uname);
		insert.setStockin_date(now1);
		insert.setStockin_remark(remark);
		
		Tb_stockinDao Stockindao = new Tb_stockinDao();
		int i = Stockindao.insert(insert);
		if (i > 0) {
			message = "添加成功!";
			 List<tb_stockin> infro = new ArrayList<tb_stockin>();
			 infro = Stockindao.queryAll();
//			 System.out.println(infro.get(infro.size()-1));
			 int stockid = infro.get(infro.size()-1).getStockin_id();   //获取入库的ID
			 tb_stockinDet inferdet = new tb_stockinDet();
			 Tb_stockinDetDao detdao = new Tb_stockinDetDao();
			 inferdet.setStockin_id(stockid);
			 inferdet.setStockindet_count(sum);
			 inferdet.setPro_id(pname);
			 int j = detdao.insert(inferdet);
			 System.out.println(j);
		} else {
			message = "添加失败";
		}
		
		 
		
		request.setAttribute("message", message);
		request.setAttribute("path", path);
		request.getRequestDispatcher("success.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
