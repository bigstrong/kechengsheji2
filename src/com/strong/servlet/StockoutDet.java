package com.strong.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.PageBean;
import com.strong.bean.StockoutdetView;
import com.strong.dao.Tb_stockoutDetDao;

/**
 * Servlet implementation class StockoutDet
 */
@WebServlet("/StockoutDet")
public class StockoutDet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockoutDet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int stockout_id=Integer.parseInt(request.getParameter("sid"));
		int page = Integer.parseInt(request.getParameter("page"));// 当前页码
		int pageSize = Integer.parseInt(request.getParameter("pageSize"));// 页面显示的大小
		PageBean<StockoutdetView> pageBean = new PageBean<StockoutdetView>();
		Tb_stockoutDetDao dao = new Tb_stockoutDetDao();
		List<StockoutdetView> list = dao.queryAllView(page, pageSize, stockout_id);
		pageBean.setCount(dao.queryAllView(stockout_id).size());
		pageBean.setList(list);
		pageBean.setPage(page);
		request.setAttribute("pageBean", pageBean);
		request.getRequestDispatcher("stock/stockoutdet.jsp").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
