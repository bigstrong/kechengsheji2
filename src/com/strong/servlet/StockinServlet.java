package com.strong.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.strong.bean.StockinView;
import com.strong.dao.Tb_stockinDao;

/**
 * Servlet implementation class StockinServlet
 */
@WebServlet("/StockinServlet")
public class StockinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StockinServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int page=Integer.parseInt(request.getParameter("page"));//当前页是 多少页(页面传过来)
		int count=0;//总共多少条记录(查询数据库)
		int pageSize=Integer.parseInt(request.getParameter("pageSize"));//每页显示多少条数据(页面传过来)
		Tb_stockinDao dao=new Tb_stockinDao();
		List<StockinView> list=dao.queryStockinView();
		count=list.size();//获取总记录数
		list=dao.queryStockinView(page, pageSize);//查询指定页的数据
		request.setAttribute("list", list);
		request.setAttribute("page", page);
		request.setAttribute("count", count);
		request.getRequestDispatcher("stock/stockin.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
