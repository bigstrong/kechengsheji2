package com.strong.dao;

import java.util.List;

import com.strong.bean.tb_product;
import com.strong.util.SqlOperation;

public class Tb_productDao {
	//添加的方法
	public int insert(tb_product tc) {
		String sql = "insert into tb_product(pro_name,size_id,color_id,outma_id,pro_facprice,pro_retprice)";
		sql = sql + " values('"+tc.getPro_name()+"','"+tc.getSize_id()+"','"+tc.getColor_id()+"','"+tc.getOutma_id()+"','"+tc.getPro_facprice()+"','"+tc.getPro_retprice()+"')";
		return SqlOperation.executeUpdate(sql);

	}
	
	//删除的方法
	public int delete(int id){
		String sql="delete from tb_product where pro_id="+id+""; 
		return SqlOperation.executeUpdate(sql);
	}
	
	//修改的方法
	public int update(tb_product tc){
		String sql = "update  tb_product set pro_name='"+tc.getPro_name()+"', size_id="+tc.getSize_id()+", color_id="+tc.getColor_id()+", outma_id="+tc.getOutma_id()+", pro_facprice="+tc.getPro_facprice()+", pro_retprice="+tc.getPro_retprice()+" where  pro_id='"+tc.getPro_id()+"'" ;
		return SqlOperation.executeUpdate(sql);
	}
	
	//查询方法
	public List<tb_product>  queryAll(){
		String sql="select * from tb_product";
		return SqlOperation.query(sql, tb_product.class);
	}
}
