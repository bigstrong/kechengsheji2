package com.strong.dao;

import java.util.List;

import com.strong.bean.tb_color;
import com.strong.util.SqlOperation;

public class Tb_colorDao {
	//添加的方法
		public int insert(tb_color tc) {
			String sql = "insert into tb_color(color_name)";
			sql = sql + " values('"+tc.getColor_name()+"')";
			return SqlOperation.executeUpdate(sql);

		}
		
		//删除的方法
		public int delete(int id){
			String sql="delete from tb_color where color_id="+id+""; 
			return SqlOperation.executeUpdate(sql);
		}
		
		//修改的方法
		public int update(tb_color tc){
			String sql = "update  tb_color set color_name='"+tc.getColor_name()+"' where  color_id='"+tc.getColor_id()+"'" ;
			return SqlOperation.executeUpdate(sql);
		}
		
		//查询方法
		public List<tb_color>  queryAll(){
			String sql="select * from tb_color";
			return SqlOperation.query(sql, tb_color.class);
		}
		//根据id进行查询的方法
		public List<tb_color>  queryById(int id){
			String sql="select * from tb_color where color_id="+id+"";
			return SqlOperation.query(sql, tb_color.class);
		}
		
		//根据name模糊查询的方法
		public List<tb_color>  queryByName(String name){
			String sql="select * from tb_color where color_name like '%"+name+"%'";
//			System.out.println(sql);
			return SqlOperation.query(sql, tb_color.class);
		}
}
