package com.strong.dao;

import com.strong.bean.tb_client;
import com.strong.util.SqlOperation;
import java.util.List;

public class Tb_clientDao {
	//添加的方法
	public int insert(tb_client tc) {
		String sql = "insert into tb_client(client_name,client_phone,client_address)";
		sql = sql + " values('"+tc.getClient_name()+"','"+tc.getClient_phone()+"','"+tc.getClient_address()+"')";
		return SqlOperation.executeUpdate(sql);

	}
	
	//删除的方法
	public int delete(int id){
		String sql="delete from tb_client where client_id="+id+""; 
		return SqlOperation.executeUpdate(sql);
	}
	
	//修改的方法
	public int update(tb_client tc){
		String sql = "update  tb_client set client_name='"+tc.getClient_name()+"',client_phone='"+tc.getClient_phone()+"',client_address='"+tc.getClient_address()+"'  where client_id='"+tc.getClient_id()+"'";
		return SqlOperation.executeUpdate(sql);
	}
	
	//查询方法
	public List<tb_client>  queryAll(){
		String sql="select * from tb_client";
		return SqlOperation.query(sql, tb_client.class);
	}
}
