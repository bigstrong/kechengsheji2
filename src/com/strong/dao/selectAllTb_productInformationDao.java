package com.strong.dao;

import java.sql.*;
import java.util.*;


import com.strong.bean.tb_product;
import com.strong.util.DataJDBC;

public class selectAllTb_productInformationDao {
	public List<tb_product> getAllTb_productInformation(){
		List<tb_product> list = new ArrayList<tb_product>();
		Connection con = DataJDBC.getConnection();//建立一个与数据库的连接
		String sql = " select * from tb_product ";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		tb_product pu = null;
		try{
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			while(rs.next()){                       							//rs 在数据库中就是一个箭头 .next()这个方法就是不断向下移动
				pu = new tb_product();
				pu.setPro_id(rs.getInt("pro_id"));
				pu.setPro_name(rs.getString("pro_name"));
				pu.setSize_id(rs.getInt("size_id"));
				pu.setColor_id(rs.getInt("color_id"));
				pu.setOutma_id(rs.getInt("outma_id"));
				pu.setPro_facprice(rs.getDouble("pro_facprice"));
				pu.setPro_retprice(rs.getDouble("pro_retprice"));
				list.add(pu);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(stmt!=null){
					stmt.close();
				}
				if(con!=null){
					con.close();
				}
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
		return list;
	}
}
