package com.strong.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.strong.bean.tb_size;
import com.strong.util.DataJDBC;

public class sqlTextDao {
	public List<tb_size> getAllTb_sizeInformation(){
		List<tb_size> list = new ArrayList<tb_size>();
		Connection con = DataJDBC.getConnection();//建立一个与数据库的连接
		String sql = " select * from tb_size ";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		tb_size pu = null;
		try{
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			while(rs.next()){                       							//rs 在数据库中就是一个箭头 .next()这个方法就是不断向下移动
				pu = new tb_size();
				pu.setSize_id(rs.getInt("size_id"));
				pu.setSize_name(rs.getString("size_name"));
				list.add(pu);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(stmt!=null){
					stmt.close();
				}
				if(con!=null){
					con.close();
				}
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
		return list;
	}
}
