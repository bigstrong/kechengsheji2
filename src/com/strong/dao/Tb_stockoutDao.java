package com.strong.dao;

import java.util.List;

import com.strong.bean.StockinView;
import com.strong.bean.StockoutView;
import com.strong.bean.tb_stockout;
import com.strong.util.SqlOperation;

public class Tb_stockoutDao {
	//添加的方法
		public int insert(tb_stockout tc) {
			String sql = "insert into tb_stockout(user_id,ware_id,client_id,stockout_remark,stockout_date)";
			sql = sql + " values("+tc.getUser_id()+","+tc.getWare_id()+","+tc.getClient_id()+",'"+tc.getStockout_remark()+"', '"+tc.getStockout_date()+"')";
			return SqlOperation.executeUpdate(sql);

		}
		
		//删除的方法
		public int delete(int id){
			String sql="delete from tb_stockout where stockout_id="+id+""; 
			return SqlOperation.executeUpdate(sql);
		}
		
		//修改的方法
		public int update(tb_stockout tc){
			String sql = "update  tb_stockout set user_id="+tc.getUser_id()+",ware_id="+tc.getWare_id()+", client_id="+tc.getClient_id()+" , stockout_date='"+tc.getStockout_date()+"' , stockout_remark='"+tc.getStockout_remark()+"' where stockout_id='"+tc.getStockout_id()+"'";
			return SqlOperation.executeUpdate(sql);
		}
		
		//查询方法
		public List<tb_stockout>  queryAll(){
			String sql="select * from tb_stockout";
			return SqlOperation.query(sql, tb_stockout.class);
		}
		   //查询出库
			public List<StockoutView> queryAllView(int page,int pageSize) {
				String sql = "select * from tb_stockout ts join tb_user tu on ts.user_id=tu.user_id join tb_client tc on ts.client_id=tc.client_id join tb_warehouse tw on ts.ware_id=tw.ware_id order by ts.stockout_id limit "+(page-1)*pageSize+" , "+pageSize+"";
//				System.out.println(sql);
				return SqlOperation.query(sql, StockoutView.class);
			}
			
			public List<StockoutView> queryAllView() {
				String sql = "select * from tb_stockout ts join tb_user tu on ts.user_id=tu.user_id join tb_client tc on ts.client_id=tc.client_id join tb_warehouse tw on ts.ware_id=tw.ware_id order by ts.stockout_id";
//				System.out.println(sql);
				return SqlOperation.query(sql, StockoutView.class);
			}
			//根据name模糊查询的方法
			   public List<StockoutView> queryByName(String name){
				   String sql="select * from tb_stockout ts join tb_warehouse tw on ts.ware_id=tw.ware_id join tb_user tu on ts.user_id=tu.user_id JOIN tb_client tc ON ts.client_id=tc.client_id where tc.client_name='"+name+"' order by ts.stockout_id";
//				   System.out.println(sql);
				   return SqlOperation.query(sql, StockoutView.class);
				}
}
