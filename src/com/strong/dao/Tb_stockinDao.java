package com.strong.dao;

import java.util.List;

import com.strong.bean.StockinView;
import com.strong.bean.tb_color;
import com.strong.bean.tb_stockin;
import com.strong.util.SqlOperation;

public class Tb_stockinDao {
	//添加的方法
	public int insert(tb_stockin tc) {
		String sql = "insert into tb_stockin(ware_id,user_id,stockin_date,stockin_remark)";
		sql = sql + " values("+tc.getWare_id()+","+tc.getUser_id()+",'"+tc.getStockin_date()+"','"+tc.getStockin_remark()+"')";
		return SqlOperation.executeUpdate(sql);

	}
	
	//删除的方法
	public int delete(int id){
		String sql="delete from tb_stockin where stockin_id="+id+""; 
		return SqlOperation.executeUpdate(sql);
	}
	
	//修改的方法
	public int update(tb_stockin tc){
		String sql = "update  tb_stockin set ware_id="+tc.getWare_id()+",user_id="+tc.getUser_id()+",stockin_date='"+tc.getStockin_date()+"' , stockin_remark='"+tc.getStockin_remark()+"' where stockin_id='"+tc.getStockin_id()+"'";
		return SqlOperation.executeUpdate(sql);
	}
	
	//查询方法
	public List<tb_stockin>  queryAll(){
		String sql="select * from tb_stockin";
		return SqlOperation.query(sql, tb_stockin.class);
	}
	//查询入库视图
	   public List<StockinView> queryStockinView(){
		   String sql="select * from tb_stockin ts join tb_warehouse tw on ts.ware_id=tw.ware_id join tb_user tu on ts.user_id=tu.user_id order by ts.stockin_id";
//		   System.out.println(sql);
		   return SqlOperation.query(sql, StockinView.class);
	   }
	   
	 //分页查询入库视图
	   public List<StockinView> queryStockinView(int page,int pageSize){
		   String sql="select * from tb_stockin ts join tb_warehouse tw on ts.ware_id=tw.ware_id join tb_user tu on ts.user_id=tu.user_id order by ts.stockin_id limit "+(page-1)*pageSize+","+pageSize+"";
//		   System.out.println(sql);
		   return SqlOperation.query(sql, StockinView.class);
	   }
		//根据name模糊查询的方法
	   public List<StockinView> queryByName(String name){
		   String sql="select * from tb_stockin ts join tb_warehouse tw on ts.ware_id=tw.ware_id join tb_user tu on ts.user_id=tu.user_id where tu.real_name='"+name+"' order by ts.stockin_id";
//		   System.out.println(sql);
		   return SqlOperation.query(sql, StockinView.class);
		}
}
