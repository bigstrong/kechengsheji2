package com.strong.dao;

import java.util.List;

import com.strong.bean.StockindetView;
import com.strong.bean.tb_stockinDet;
import com.strong.util.SqlOperation;

public class Tb_stockinDetDao {
	//添加的方法
	public int insert(tb_stockinDet tc) {
		String sql = "insert into tb_stockinDet(stockin_id,stockindet_count,pro_id)";
		sql = sql + " values("+tc.getStockin_id()+","+tc.getStockindet_count()+","+tc.getPro_id()+")";
		return SqlOperation.executeUpdate(sql);

	}
	
	//删除的方法
	public int delete(int id){
		String sql="delete from tb_stockinDet where stockindet_id="+id+""; 
		return SqlOperation.executeUpdate(sql);
	}
	
	//修改的方法
	public int update(tb_stockinDet tc){
		String sql = "update  tb_stockinDet set stockin_id="+tc.getStockin_id()+",stockindet_count="+tc.getStockindet_count()+",pro_id="+tc.getPro_id()+"  where stockindet_id="+tc.getStockindet_id()+"";
		return SqlOperation.executeUpdate(sql);
	}
	
	//查询方法
	public List<tb_stockinDet>  queryAll(){
		String sql="select * from tb_stockinDet";
		return SqlOperation.query(sql, tb_stockinDet.class);
	}
	//查询入库详细视图
	   public List<StockindetView> queryStockinView(int stock_id){
		   String sql="select * from tb_stockindet ts join tb_product tp on ts.pro_id=tp.pro_id where stockin_id="+stock_id+" order by ts.stockindet_id";
//		   System.out.println(sql);
		   return SqlOperation.query(sql, StockindetView.class);
	   }
	   
}
