package com.strong.dao;

import java.util.List;

import com.strong.bean.StockoutdetView;
import com.strong.bean.tb_stockoutdet;
import com.strong.util.SqlOperation;

public class Tb_stockoutDetDao {
	//添加的方法
	public int insert(tb_stockoutdet tc) {
		String sql = "insert into tb_stockoutdet(pro_id,stockout_id,stockoutdet_count)";
		sql = sql + " values("+tc.getPro_id()+","+tc.getStockout_id()+","+tc.getStockoutdet_count()+")";
		return SqlOperation.executeUpdate(sql);

	}
	
	//删除的方法
	public int delete(int id){
		String sql="delete from tb_stockoutdet where stockoutdet_id="+id+""; 
		return SqlOperation.executeUpdate(sql);
	}
	
	//修改的方法
	public int update(tb_stockoutdet tc){
		String sql = "update  tb_stockoutdet set pro_id="+tc.getPro_id()+",stockout_id="+tc.getStockout_id()+",stockoutdet_count="+tc.getStockoutdet_count()+"  where stockoutdet_id="+tc.getStockoutdet_id()+"";
		return SqlOperation.executeUpdate(sql);
	}
	
	//查询方法
	public List<tb_stockoutdet>  queryAll(){
		String sql="select * from tb_stockoutdet";
		return SqlOperation.query(sql, tb_stockoutdet.class);
	}
	//查询库存详情
	public List<StockoutdetView> queryAllView(int stockout_id) {
		String sql = "select * from tb_stockoutdet ts  join tb_product tp on ts.pro_id=tp.pro_id where ts.stockout_id="+stockout_id+" ORDER BY ts.stockoutdet_id ";
//		System.out.println(sql);
		return SqlOperation.query(sql, StockoutdetView.class);
	}
	
	
	//分页查询
	public List<StockoutdetView> queryAllView(int page,int pageSize,int stockout_id) {
		String sql = "select * from tb_stockoutdet ts  join tb_product tp on ts.pro_id=tp.pro_id where ts.stockout_id="+stockout_id+" ORDER BY ts.stockoutdet_id  limit "+(page-1)*pageSize+" , "+pageSize+"";
//		System.out.println(sql);
		return SqlOperation.query(sql, StockoutdetView.class);
	}
}
