package com.strong.dao;

import java.util.List;

import com.strong.bean.tb_warehouse;
import com.strong.util.SqlOperation;

public class Tb_warehouseDao {
	//添加的方法
	public int insert(tb_warehouse tc) {
		String sql = "insert into tb_warehouse(ware_name,ware_linkman,ware_linkphone,ware_content)";
		sql = sql + " values('"+tc.getWare_name()+"','"+tc.getWare_linkman()+"','"+tc.getWare_linkphone()+"', "+tc.getWare_content()+")";
		return SqlOperation.executeUpdate(sql);

	}
	
	//删除的方法
	public int delete(int id){
		String sql="delete from tb_warehouse where ware_id="+id+""; 
		return SqlOperation.executeUpdate(sql);
	}
	
	//修改的方法
	public int update(tb_warehouse tc){
		String sql = "update  tb_warehouse set ware_name='"+tc.getWare_name()+"',ware_linkman='"+tc.getWare_linkman()+"',ware_linkphone='"+tc.getWare_linkphone()+"' , ware_content= "+tc.getWare_content()+" where ware_id="+tc.getWare_id()+"";
		return SqlOperation.executeUpdate(sql);
	}
	
	//查询方法
	public List<tb_warehouse>  queryAll(){
		String sql="select * from tb_warehouse";
		return SqlOperation.query(sql, tb_warehouse.class);
	}
}
