package com.strong.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SqlOperation {
	// 关闭资源的方法
		public static void closeAll(Connection cn, Statement st, ResultSet rs) {
			try {
				if (cn != null)
					cn.close();
				if (st != null)
					st.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// 通用增删改的方法
		public static int executeUpdate(String sql,Object...pars) {
			Connection cn = DataJDBC.getConnection();
			PreparedStatement st = null;
			int count = 0;
			try {

				st = cn.prepareStatement(sql);

				if (pars != null) {
					// 使用循环来设置参数
					for (int i = 0; i < pars.length; i++) {
						st.setObject(i + 1, pars[i]);
					}
				}

				count = st.executeUpdate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				SqlOperation.closeAll(cn, st, null);
			}
			return count;
		}
		// 通用的查询方法
		public static <T> List<T> query(String sql, Class<T> cls, Object... obj) {
			List<T> list = new ArrayList<T>();// 创建一个集合
			Connection cn = DataJDBC.getConnection();
			PreparedStatement pst = null;
			ResultSet rs = null;
			try {
				pst = cn.prepareStatement(sql);
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						pst.setObject(i + 1, obj[i]);
					}
				}
				rs = pst.executeQuery();
				while (rs.next()) {
					T t = cls.newInstance();// 利用无参的构造方法创建对象
					// 利用set方法初始化对象
					Field[] fd = cls.getDeclaredFields();// 获取该类的所有的属性 
					int n = fd.length;// 当前类的属性个数
					// 如果父类不是object 那么获取父类的属性个数 并且将当前类和父类的数组进行合并
					if (!cls.getSuperclass().equals(Object.class)) {
						Field[] fdp = cls.getSuperclass().getDeclaredFields();
						fd = Arrays.copyOf(fd, fd.length + fdp.length);
						System.arraycopy(fdp, 0, fd, n, fdp.length);//数组合并
					}
					for (int i = 0; i < fd.length; i++) {
						// 通过属性获取对应的set方法
						String fieldName = fd[i].getName();// 获取属性的名字 user_id
						String methodName = "set"
								+ fieldName.substring(0, 1).toUpperCase()
								+ fieldName.substring(1);// 拼接得到对应属性的set方法名
						Method method = null;
						// 子类的set方法
						if (i < n) {
							method = cls.getDeclaredMethod(methodName,
									fd[i].getType()); // 通过方法名和属性的数据类型 获取对应set方法
						}
						// 父类的set方法
						else {
							method = cls.getSuperclass().getDeclaredMethod(
									methodName, fd[i].getType());
						}
						Object value = null;
						if (fd[i].getType().equals(int.class)) {
							value = rs.getInt(fieldName);
						}
						if (fd[i].getType().equals(double.class)) {
							value = rs.getDouble(fieldName);
						}
						if (fd[i].getType().equals(String.class)) {
							value = rs.getString(fieldName);
						}
						if (fd[i].getType().equals(Date.class)) {
							value = rs.getDate(fieldName);
						}
						method.invoke(t, value);
					}
					list.add(t);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				SqlOperation.closeAll(cn, pst, rs);
			}
			return list;
		}
	}