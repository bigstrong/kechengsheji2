package com.strong.bean;

import java.io.Serializable;

public class tb_warehouse implements Serializable{
		private int ware_id;
		private String ware_name;
		private String ware_linkman;
		private String ware_linkphone;
		private int ware_content;
		
		public tb_warehouse() {}

		public int getWare_id() {
			return ware_id;
		}

		public void setWare_id(int ware_id) {
			this.ware_id = ware_id;
		}

		public String getWare_name() {
			return ware_name;
		}

		public void setWare_name(String ware_name) {
			this.ware_name = ware_name;
		}

		public String getWare_linkman() {
			return ware_linkman;
		}

		public void setWare_linkman(String ware_linkman) {
			this.ware_linkman = ware_linkman;
		}

		public String getWare_linkphone() {
			return ware_linkphone;
		}

		public void setWare_linkphone(String ware_linkphone) {
			this.ware_linkphone = ware_linkphone;
		}

		public int getWare_content() {
			return ware_content;
		}

		public void setWare_content(int ware_content) {
			this.ware_content = ware_content;
		}

		public tb_warehouse(int ware_id, String ware_name, String ware_linkman, String ware_linkphone,
				int ware_content) {
			super();
			this.ware_id = ware_id;
			this.ware_name = ware_name;
			this.ware_linkman = ware_linkman;
			this.ware_linkphone = ware_linkphone;
			this.ware_content = ware_content;
		}

		
}
