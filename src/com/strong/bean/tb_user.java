package com.strong.bean;

import java.io.Serializable;

public class tb_user implements Serializable{
		private int user_id;
		private String user_name;
		private String user_pass;
		private String real_name;
		private String user_sex;
		private String user_mobil;
		
		public tb_user() {}

		public int getUser_id() {
			return user_id;
		}

		public void setUser_id(int user_id) {
			this.user_id = user_id;
		}

		public String getUser_name() {
			return user_name;
		}

		public void setUser_name(String user_name) {
			this.user_name = user_name;
		}

		public String getUser_pass() {
			return user_pass;
		}

		public void setUser_pass(String user_pass) {
			this.user_pass = user_pass;
		}

		public String getReal_name() {
			return real_name;
		}

		public void setReal_name(String real_name) {
			this.real_name = real_name;
		}

		public String getUser_sex() {
			return user_sex;
		}

		public void setUser_sex(String user_sex) {
			this.user_sex = user_sex;
		}

		public String getUser_mobil() {
			return user_mobil;
		}

		public void setUser_mobil(String user_mobil) {
			this.user_mobil = user_mobil;
		}

		public tb_user(int user_id, String user_name, String user_pass, String real_name, String user_sex,
				String user_mobil) {
			super();
			this.user_id = user_id;
			this.user_name = user_name;
			this.user_pass = user_pass;
			this.real_name = real_name;
			this.user_sex = user_sex;
			this.user_mobil = user_mobil;
		}
		
		
}
