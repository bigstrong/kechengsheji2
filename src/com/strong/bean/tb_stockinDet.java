package com.strong.bean;

import java.io.Serializable;

public class tb_stockinDet implements Serializable{
		private int stockindet_id;
		private int stockin_id;
		private int stockindet_count;
		private int pro_id;
		
		public tb_stockinDet() {}

		public int getStockindet_id() {
			return stockindet_id;
		}

		public void setStockindet_id(int stockindet_id) {
			this.stockindet_id = stockindet_id;
		}

		public int getStockin_id() {
			return stockin_id;
		}

		public void setStockin_id(int stockin_id) {
			this.stockin_id = stockin_id;
		}

		public int getStockindet_count() {
			return stockindet_count;
		}

		public void setStockindet_count(int stockindet_count) {
			this.stockindet_count = stockindet_count;
		}

		public int getPro_id() {
			return pro_id;
		}

		public void setPro_id(int pro_id) {
			this.pro_id = pro_id;
		}

		public tb_stockinDet(int stockindet_id, int stockin_id, int stockindet_count, int pro_id) {
			super();
			this.stockindet_id = stockindet_id;
			this.stockin_id = stockin_id;
			this.stockindet_count = stockindet_count;
			this.pro_id = pro_id;
		}
		
		
}
