package com.strong.bean;

import java.io.Serializable;

public class tb_stockoutdet implements Serializable{
		private int stockoutdet_id;
		private int pro_id;
		private int stockout_id;
		private int stockoutdet_count;
		
		public tb_stockoutdet() {}

		public int getStockoutdet_id() {
			return stockoutdet_id;
		}

		public void setStockoutdet_id(int stockoutdet_id) {
			this.stockoutdet_id = stockoutdet_id;
		}

		public int getPro_id() {
			return pro_id;
		}

		public void setPro_id(int pro_id) {
			this.pro_id = pro_id;
		}

		public int getStockout_id() {
			return stockout_id;
		}

		public void setStockout_id(int stockout_id) {
			this.stockout_id = stockout_id;
		}

		public int getStockoutdet_count() {
			return stockoutdet_count;
		}

		public void setStockoutdet_count(int stockoutdet_count) {
			this.stockoutdet_count = stockoutdet_count;
		}

		public tb_stockoutdet(int stockoutdet_id, int pro_id, int stockout_id, int stockoutdet_count) {
			super();
			this.stockoutdet_id = stockoutdet_id;
			this.pro_id = pro_id;
			this.stockout_id = stockout_id;
			this.stockoutdet_count = stockoutdet_count;
		}
		
		
}
