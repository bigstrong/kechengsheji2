package com.strong.bean;

import java.io.Serializable;

public class tb_outma implements Serializable{
		private int outma_id;
		private String outma_name;
		
		public tb_outma() {}

		public int getOutma_id() {
			return outma_id;
		}

		public void setOutma_id(int outma_id) {
			this.outma_id = outma_id;
		}

		public String getOutma_name() {
			return outma_name;
		}

		public void setOutma_name(String outma_name) {
			this.outma_name = outma_name;
		}

		public tb_outma(int outma_id, String outma_name) {
			super();
			this.outma_id = outma_id;
			this.outma_name = outma_name;
		}
		
		
}
